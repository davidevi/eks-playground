# EKS Playground

1. Create 'env' file with required var exports:
```shell
export TERRAFORM_STATE_BUCKET=davide-terraform-state
```
2. Make `cli` executable
```shell
chmod +x cli
```
3. Run `cli` (Output might not match example below)
```shell
$ ./cli
Usage: ./cli <command>
Commands:
  init             Runs terraform init with loaded configuration
  plan             Runs terraform plan with loaded configuration
  apply            Runs terraform apply with loaded configuration
  destroy          Runs terraform destroy with loaded configuration
```
4. Run commands via `cli`:
```shell
./cli init
```