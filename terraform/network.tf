
# region Networking ============================================================

resource "aws_vpc" "cluster_vpc" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name = "${var.ENVIRONMENT}-eks-playground-vpc"
  }
}

resource "aws_subnet" "cluster_subnet_a" {
  vpc_id                  = aws_vpc.cluster_vpc.id
  availability_zone       = "${var.AWS_REGION}a"
  cidr_block              = "10.0.0.0/24"
  map_public_ip_on_launch = true

  tags = {
    Name = "${var.ENVIRONMENT}-eks-playground-subnet-a"
  }
}

resource "aws_subnet" "cluster_subnet_b" {
  vpc_id                  = aws_vpc.cluster_vpc.id
  availability_zone       = "${var.AWS_REGION}b"
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = true

  tags = {
    Name = "${var.ENVIRONMENT}-eks-playground-subnet-b"
  }
}

resource "aws_internet_gateway" "main_gateway" {
  vpc_id = aws_vpc.cluster_vpc.id

  tags = {
    Name = "${var.ENVIRONMENT}-eks-playground-igw"
  }

}

resource "aws_route_table" "internet_routing" {
  vpc_id = aws_vpc.cluster_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main_gateway.id
  }
}

resource "aws_route_table_association" "subnet_a" {
  subnet_id      = aws_subnet.cluster_subnet_a.id
  route_table_id = aws_route_table.internet_routing.id
}

resource "aws_route_table_association" "subnet_b" {
  subnet_id      = aws_subnet.cluster_subnet_b.id
  route_table_id = aws_route_table.internet_routing.id
}

resource "aws_eip" "lb1" {
  depends_on = [aws_internet_gateway.main_gateway]
}

resource "aws_nat_gateway" "cluster_nat_gateway_a" {
  allocation_id = aws_eip.lb1.id
  subnet_id     = aws_subnet.cluster_subnet_a.id
  depends_on = [
    aws_internet_gateway.main_gateway
  ]

  tags = {
    Name = "${var.ENVIRONMENT}-eks-playground-nat-gateway-a"
  }
}

resource "aws_eip" "lb2" {
  depends_on = [aws_internet_gateway.main_gateway]
}

resource "aws_nat_gateway" "cluster_nat_gateway_b" {
  allocation_id = aws_eip.lb2.id
  subnet_id     = aws_subnet.cluster_subnet_b.id
  depends_on = [
    aws_internet_gateway.main_gateway
  ]

  tags = {
    Name = "${var.ENVIRONMENT}-eks-playground-nat-gateway-b"
  }
}

# endregion ====================================================================
