terraform {

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }

  backend "s3" {
    # Configured during `terraform init` so that it can be parameterised
    # See the `deploy` task for more information
    key = "terraform.tfstate"
  }

}

provider "aws" {}
