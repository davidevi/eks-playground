
variable "AWS_REGION" {
  description = "The AWS region to deploy to"
  type        = string
}

variable "ENVIRONMENT" {
  description = "The environment to deploy to"
  type        = string
}
